
#include "Matrix.h"
#include <iostream>
using namespace std;

// Default Constructor (empty body, per requirements, due to initialization in header file)
Matrix::Matrix() { }

// Virtual function to allow for redefining in the SqMatrix class
void Matrix::printDiagonal() const
{
   cout << "Matrix::printDiagonal(): ";
   cout << "Sorry, no diagonal\n\n";
}

// Copy Constructor
Matrix::Matrix( const Matrix& m2 )
{
   // Set the values for rows and columns equal to the argument's values
   numRows = m2.numRows;
   numCols = m2.numCols;
   
   // Allocate space for the new array
   this->allocateSpace();
   cout << "Matrix copy ctor: Allocated the matrix\n";
   //mArray = new double*[numRows];
   
   for(int i = 0; i < m2.numRows; i++)
   {
      //mArray[i] = new double[numCols];
      
      for(int j = 0; j < m2.numCols; j++)
      {
	 // Set the values for the new Matrix equal to that of the arguements
	 mArray[i][j] = m2.mArray[i][j];
      }
   }
   
}

// Deallocate space for a matrix
const Matrix::~Matrix()
{
   //cout << "\nDeleted a " << numRows << " by " << numCols << " Matrix\n";
   cout << "Matrix dtor: Released the matrix" << endl << endl;
   deallocateSpace();
}

// Adds two matrices together, return result (sum) by value
const Matrix operator+( const Matrix& m1, const Matrix& m2 )// const
{
   if(m1.numRows != m2.numRows || m1.numCols != m2.numCols)
   {
      cerr << "INCORRECT DIMENSIONS! CAN NOT OPERATE UNDER ADDITION" << endl;
      cerr << "Returning the LHS of the addition operator" << endl << endl;
      return m1;
   }
   
   // Create the holder for the sum, and allocate it's space
   Matrix sum;
   sum.numRows = m1.numRows;
   sum.numCols = m1.numCols;
   sum.allocateSpace();
   
   for(int i = 0; i < sum.numRows; i++)
   {
      for(int j = 0; j < sum.numCols; j++)
      {
	 // Do the addition and store it in the sum object
	 sum.mArray[i][j] = m1.mArray[i][j] + m2.mArray[i][j];
      }
   }
   
   return sum;
}

// Sets LHS equal to the RHS (Cascading is allowed)
const Matrix& Matrix::operator=( const Matrix& m2 )
{
   // If the matrices are the same then Instructor said it is fine
   //to just copy the values over (repeating the values).
   /*if(this == &m2)
   { return *this; }*/
   
   // See if we need to get fresh space for the calling object
   if(numRows != m2.numRows || numCols != m2.numCols)
   {
      deallocateSpace();
      numRows = m2.numRows;
      numCols = m2.numCols;
      allocateSpace();
   }
   
   // Once the appropriate amount of space is confirmed available,
   //begin assigning the new matrix values to the RHS matrix values
   for(int i = 0; i < numRows; i++)
   {      
      for(int j = 0; j < numCols; j++)
      { mArray[i][j] = m2.mArray[i][j]; }
   }
   
   cout << "Matrix operator=: Assigned the matrix" << endl;
   
   // Return the adjusted calling object
   return *this;
}

// Insertion Operator (ostream)
ostream& operator<<( ostream& outputStream, const Matrix& m1 )
{
   outputStream << "This is a " << m1.numRows << " by " << m1.numCols
	        << " Matrix" << endl;
		
   // Output the matrix row by row
   for(int i = 0; i < m1.numRows; i++)
   {
      for(int j = 0; j < m1.numCols; j++)
      {
	 outputStream << m1.mArray[i][j];
	 outputStream << " ";
      }
      outputStream << endl;
   }
   
   //outputStream << endl;
   outputStream.flush();
}

// Extraction Operator (istream)
istream& operator>>( istream& inputStream, Matrix& m1 )
{
   // Per requirements, perhaps to ensure the matrix is at
   //first empty with default values.
   m1.deallocateSpace();
   
   inputStream >> m1.numRows;
   inputStream >> m1.numCols;
   
   m1.allocateSpace();
   
   // Load 'em up.
   for(int i = 0; i < m1.numRows; i++)
   {
      for(int j = 0; j < m1.numCols; j++)
      {
	 inputStream >> m1.mArray[i][j];
      }
   }
   inputStream.ignore(1000, '\n');
}

// Allocate space for a matrix
const void Matrix::allocateSpace()
{
   mArray = new double*[numRows];
   for(int i = 0; i < numRows; i++)
   {
      mArray[i] = new double[numCols];
   }
}

// Deallocate space for a matrix
const void Matrix::deallocateSpace()
{
   for(int i = 0; i < numRows; i++)
   {
      delete[] mArray[i];
   }
   
   delete mArray;
   
   numRows = 0;
   numCols = 0;
   
}
