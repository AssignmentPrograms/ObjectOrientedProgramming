/*
 * Name: Brandon Jones
 * WSU ID: N534H699
 * WSU Email ID: N534H699
 * */

#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
using namespace std;

class Matrix
{
protected:
   int numRows = 0; // number of rows
   int numCols = 0; // number of cols
   double** mArray = nullptr;
   
public:
   Matrix();
   
   // Copy Constructor
   Matrix( const Matrix& m2 );
   
   //-----------------------------------------------------
   // NOTE: These were included before deciding to make (B-Class) dmembers private.
   //Will keep here in order for completeness
   //You may see the calls to these commented out in SqMatrix.cpp
   //This is not a problem.
   
   /*int getNumRows() const
   { return numRows; }
   
   int getNumCols() const
   { return numCols; }
   
   void setNumRows( const int& rows )
   { numRows = rows; }
   
   void setNumCols( const int& cols )
   { numCols = cols; }*/
   
   /*void setmArray( int value, int i, int j )
   {
      if( i == 0 && j == 0)
      { allocateSpace(); }
      
      mArray[i][j] = value;
   }*/

   
   //-----------------------------------------------------
   
   // Deallocate space for a matrix
   virtual const ~Matrix();
   
   // Adds two matrices together
   friend const Matrix operator+( const Matrix& m1, const Matrix& m2 ); //const;
   
   // Assignmnet Operator Overload
   const Matrix& operator=( const Matrix& m2 );
   
   // Insertion Operator (ostream)
   friend ostream& operator<<( ostream& outputStream, const Matrix& m1 );
   
   // Extraction Operator (istream)
   friend istream& operator>>( istream& inputStream, Matrix& m1 );

   // Virtual function to allow for redefining in the SqMatrix class
   //virtual void PrintDiagonal() const;
   virtual void printDiagonal() const;
   
   // Allocate space for a matrix
   const void allocateSpace();
   
   // Deallocate space for a matrix
   const void deallocateSpace();
};


#endif