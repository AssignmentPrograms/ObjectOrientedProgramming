/*
 * Name: Brandon Jones
 * WSU ID: N534H699
 * WSU Email ID: N534H699
 * */

#ifndef SQMATRIX_H
#define SQMATRIX_H

#include "Matrix.h"

class SqMatrix : public Matrix
{
   double* diagonal = nullptr;
   
public:
   // A default constructor
   SqMatrix();
   
   // Copy constructor
   SqMatrix( const SqMatrix& sqm1 );
   
   // Assingment Operator overload
   const SqMatrix& operator=( const SqMatrix& sqm1 );
   
   // Destructor
   const ~SqMatrix();
   
   // Overload for the extraction operator
   friend istream& operator>>( istream& inputStream, SqMatrix& sqm1 );
   
   // Overload for the insertion operator
   friend ostream& operator<<( ostream& outputStream, const SqMatrix& sqm1 );
   
   virtual void printDiagonal() const;
};


#endif