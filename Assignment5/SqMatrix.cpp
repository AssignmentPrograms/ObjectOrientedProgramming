

#include "SqMatrix.h"
#include <iostream>
using namespace std;


// Default Constructor, calls the Matrix default constructor as well
// Calls the Matrix Default Constructor first and then calls the SqMatrix Constructor
// After creating a SqMatrix a matrix with appropriate data
//fields should be filled
SqMatrix::SqMatrix() : Matrix()
{ }

void SqMatrix::printDiagonal() const
{
   cout << "SqMatrix::printDiagonal(): ";
   
   int size = numCols;//this->getNumCols();
   
   for(int i = 0; i < size; i++)
   { cout << diagonal[i] << " "; }
   cout << endl << endl;
}

// Copy Constructor
// Calls Base Class Copy Constructor, do so Matrix default constructor does not get called
SqMatrix::SqMatrix( const SqMatrix& orig) : Matrix( orig )
{
   // Called the B-Class copy constructor so there will be values populated here
   int rows = numRows;//orig.getNumRows();
   int cols = numCols;//orig.getNumCols();
   
   // For readability, size is simply the square matrix rows OR cols
   int size = cols;
   diagonal = new double[size];
   
   cout << "SqMatrix copy ctor: Allocated the diagonal\n";
   
   for(int i = 0; i < rows; i++ )
   {
      for(int j = 0; j < cols; j++ )
      {
	 // If we are at a location along the diagonal
	 if( i == j )
	 {
	    diagonal[i] = orig.diagonal[i];
	 }
      }
   }
}

// Assignmnet Operator overload
const SqMatrix& SqMatrix::operator=( const SqMatrix& sqm2 )
{
   Matrix::operator=( sqm2 );
   
   int cols = numCols;//sqm2.getNumCols();
   int rows = numRows;//sqm2.getNumRows();
   
   if( rows == cols )
   {
      int size = cols;
      
      if( this->diagonal != nullptr )
      {
	 delete[] diagonal;
      }
      diagonal = new double[size];
	 
      for(int i = 0; i < size; i++)
      {
	 diagonal[i] = sqm2.diagonal[i];
      }
   }
   else
   {
      cout << "This matrix has " << rows << " rows and " << cols
      << " columns" << " and is therefore not a square matrix." << endl;
   }
   cout << "SqMatrix operator=: Assigned the diagonal" << endl;
   
   return *this;
}

// Overload for the extraction operator
istream& operator>>( istream& inputStream, SqMatrix& sqm1 )
{
   int rows;
   int cols;
   
   /*inputStream >> rows;
   sqm1.setNumRows(rows);*/
   inputStream >> sqm1.numRows;
   
   /*inputStream >> cols;
   sqm1.setNumCols(cols);*/
   inputStream >> sqm1.numCols;
   
   sqm1.allocateSpace();
   
   rows = sqm1.numRows;
   cols = sqm1.numCols;
   
   if( cols == rows )
   {
      int size = cols;
      int value;
   
      sqm1.diagonal = new double[size];
      for(int i = 0; i < size; i++)
      {
	 for(int j = 0; j < size; j++)
	 {
	    inputStream >> value;
	    //sqm1.setmArray( value, i, j );
	    sqm1.mArray[i][j] = value;
	    
	    if(i == j)
	    {
	       sqm1.diagonal[i] = value;
	    }
	 }
	 inputStream.ignore(1000, '\n');
      }
   }
   else
   {
      cout << "This matrix has " << rows << " rows and " << cols
      << " columns" << " and is therefore not a square matrix." << endl;
   }
   
   return inputStream;
}

// Overload for the insertion operator
ostream& operator<<( ostream& outputStream, const SqMatrix& sqm1 )
{
   int rows = sqm1.numRows;//sqm1.getNumRows();
   int cols = sqm1.numCols;//sqm1.getNumCols();
   
   if( cols == rows )
   {
      int size = cols;
      outputStream << "a square matrix of size " << size << endl;
      
      //sqm1.getmArray();
      
      for(int i = 0; i < size; i++)
      {
	 for(int j = 0; j < size; j++)
	 {
	    outputStream << sqm1.mArray[i][j] << " ";//getmArrayValue( i, j ) << " ";
	 }
	 cout << endl;
      }
      
      outputStream << "Its diagonal is: ";
      for(int i = 0; i < size; i++)
      {
	 outputStream << sqm1.diagonal[i] << " ";
      }
      cout << endl;
   }
   else
   {
      cout << "This matrix has " << rows << " rows and " << cols
      << " columns" << " and is therefore not a square matrix." << endl;
   }
   cout << endl;
   return outputStream;
}

// Destructor
// This automatically calls the B-Class Deconstructor, does so after this destructor.
const SqMatrix::~SqMatrix()
{
   cout << "SqMatrix dtor: Released the diagonal" << endl;
   delete[] diagonal;
}