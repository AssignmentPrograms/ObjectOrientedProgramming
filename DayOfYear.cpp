#include <iostream>
#include <cstdlib>
#include "DayOfYear.h"
using namespace std;



// Constructor that takes in a month and a day value
DayOfYear::DayOfYear(int monthValue, int dayValue) : month(monthValue), day(dayValue)
{ testDate(); }

// Constructor that takes in a month value (initializes day to 0)
DayOfYear::DayOfYear(int monthValue) : month(monthValue), day(1)
{ testDate(); }

// Constructor that initalizes both month and day values to 0
DayOfYear::DayOfYear() : month(1), day(1)
{ testDate(); }

// Allows for user to enter in a month and a day
void DayOfYear::input()
{
    cout << "Enter in the Month number: ";
    cin >> month;
    cout << "Enter in the Day number: ";
    cin >> day;

    testDate();
}

// Print out the values of month and day in a paticular format
void DayOfYear::output() const
{
    // Determine the current month in question
    switch(month)
    {
        case 1:
            cout << "January "; break;
        case 2:
            cout << "February "; break;
        case 3:
            cout << "March "; break;
        case 4:
            cout << "April "; break;
        case 5:
            cout << "May "; break;
        case 6:
            cout << "June "; break;
        case 7:
            cout << "July "; break;
        case 8:
            cout << "August "; break;
        case 9:
            cout << "September "; break;
        case 10:
            cout << "October "; break;
        case 11:
            cout << "November "; break;
        case 12:
            cout << "December "; break;
        default:
            cout << "Error in DayOfYear::output";
    }

    // Print out the day that goes with the selected month
    cout << day << endl;
}

void DayOfYear::testDate() const
{
    if(month < 1 || month > 12)
    { cout << "Illegal Month Value\n"; exit(1); }
    if(day < 1 || day > 30)
    { cout << "Illegal Day Value\n"; exit(1); }
}

// Return the object's month number
int DayOfYear::getMonthNumber() const
{   return month; }


// Return the object's day number
int DayOfYear::getDay() const
{   return day; }

// TODO ERROR HANDLING FOR INVALID RESULTS
DayOfYear operator +(DayOfYear &date1, DayOfYear &date2)
{
    return DayOfYear( (date1.month + date2.month), (date1.day + date2.day) );
}

// TODO ERROR HANDLING FOR INVALID RESULTS
DayOfYear operator -(DayOfYear &date1, DayOfYear &date2)
{
    return DayOfYear( (date1.month - date2.month), (date1.day - date2.day) );
}

// TODO ERROR HANDLING FOR INVALID RESULTS
DayOfYear operator -(DayOfYear &date)
{
    DayOfYear d(12, 30);
    date = d - date;
}

// TODO ERROR HANDLING FOR INVALID RESULTS
bool operator ==(DayOfYear &date1, DayOfYear &date2)
{
    return ( (date1.month == date2.month) && (date1.day == date2.day) );
}

// TODO ERROR HANDLING FOR INVALID RESULTS
bool operator <(DayOfYear &date1, DayOfYear &date2)
{
    return ( (date1.month < date2.month) || ( (date1.month == date2.month) && (date1.day < date2.day) ) );
}
