#ifndef DAYOFYEAR_H
#define DAYOFYEAR_H



class DayOfYear
{
    int month;
    int day;
    void testDate() const;

public:
    // Constructor that takes in a month and a day value
    DayOfYear(int monthValue, int dayValue);

    // Constructor that takes in a month value (initializes day to 0)
    DayOfYear(int monthValue);

    // Constructor that initalizes both month and day values to 0
    DayOfYear();

    // Allows for user to enter in a month and a day
    void input();

    // Print out the values of month and day in a paticular format
    void output() const;

    // Return the object's month number
    int getMonthNumber() const;

    // Return the object's day number
    int getDay() const;

    friend DayOfYear operator +(DayOfYear &date1, DayOfYear &date2);

    friend DayOfYear operator -(DayOfYear &date1, DayOfYear &date2);

    friend DayOfYear operator -(DayOfYear &date);

    friend bool operator ==(DayOfYear &date1, DayOfYear &date2);

    friend bool operator <(DayOfYear &date1, DayOfYear &date2);

    //friend DayOfYear operator ++(DayOfYear date1, DayOfYear date2);

    //friend DayOfYear operator ++(DayOfYear date1, DayOfYear date2);

    //friend DayOfYear operator --(DayOfYear date1, DayOfYear date2);

    //friend DayOfYear operator --(DayOfYear date1, DayOfYear date2);

    friend DayOfYear operator <<(DayOfYear &date1, DayOfYear &date2);

    friend DayOfYear operator >>(DayOfYear &date1, DayOfYear &date2);

    //friend DayOfYear operator [](DayOfYear date1, DayOfYear date2);

};

#endif
