

#ifndef CPPSTRING_H
#define CPPSTRING_H

#include "CString.h"

#include <string>
#include <iostream>
using namespace std;

class CppString
{
    int numStrings;
    string *sArray;

public:

    // Default Constructor
    CppString();

    // Constructor
    CppString(CString& c);

    // Deconstructor
    ~CppString();
    
    // Returns numStrings;
    const int getNumStrings();

    // Sorts each string with respect to the other strings
    void sort();
   
    // Insertion operator to allow for custom output of CppString objects
    friend ostream& operator<<(ostream& outputStream, const CppString& sString);

};

#endif
