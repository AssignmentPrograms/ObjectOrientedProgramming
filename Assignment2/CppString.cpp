

#include "CppString.h"

#include "CString.h"

#include <iostream>
#include <string>
#include <cstring>
using namespace std;

// GOOD
CppString::CppString()
{
    numStrings = 0;
    sArray = nullptr;
}

const int CppString::getNumStrings() { return numStrings; }

// CString Copy Constructor
// GOOD
CppString::CppString(CString& c)
{
    numStrings = c.getNumStrings();

    sArray = new string[numStrings];

    for(int i = 0; i < numStrings; i++)
    {
        sArray[i] = c[i];
    }
}

// GOOD
CppString::~CppString()
{
    cout << "This is the destructor for CppString" << endl;
    
    cout << "Releasing the space allocated for sArray" << endl << endl;
    delete[] sArray;
}

// GOOD
void CppString::sort()
{
   string temp;
   
   for(int i = 1; i < numStrings; i++)
      for(int j = 0; j < numStrings - i; j++)
      {
	 if(sArray[j] > sArray[j+1])
	 {
	    temp = sArray[j];
	    sArray[j] = sArray[j+1];
	    sArray[j+1] = temp;
	 }
      }
}

// GOOD
ostream& operator<<(ostream& outputStream, const CppString& S1)
{
    outputStream << "S1 contains " << S1.numStrings << " strings" << endl;

    for(int i = 0; i < S1.numStrings; i++)
    {
        outputStream << S1.sArray[i];
	cout << endl;
    }
    
    return outputStream;
}
