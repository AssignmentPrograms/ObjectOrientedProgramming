#include<iostream>
#include<string>
using namespace std;

#include "CString.h"
#include "CppString.h"

int main()
{  CString c1;

   cin >> c1;

   cout << "c1 contains:\n";
   cout << c1 << endl;

   CString c2(c1);
   cout << "c2 contains:\n";
   cout << c2 << endl;

   c2.sort();
   cout << "After sorting, c2 contains:\n";
   cout << c2 << endl;

   CppString s(c1);
   cout << "s contains:\n";
   cout << s << endl;

   s.sort();
   cout << "After sorting, s contains:\n";
   cout << s << endl;
}
