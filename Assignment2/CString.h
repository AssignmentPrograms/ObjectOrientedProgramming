/***********************************
NAME: Brandon Jones
WSU Login ID: N534H699
myWSUID: N534H699
***********************************/

#ifndef CSTRING_H
#define CSTRING_H

#include <iostream>
using namespace std;

class CString
{
   int numStrings = 0;
   char **cArray;

public:
   // Default Constructor
   CString();

   // Copy Constructor
   CString(const CString& C2);

   // Destructor
   ~CString();

   // Returns numStrings;
   const int getNumStrings();

   // Sorts the strings using bubblesort
   void sort();

   // Returns a poitner to the ith string in the array
   // Returns nullptr if i is out of range
   char* operator[](const int index);

   friend istream& operator>>(istream& inputStream, CString& C1);

   friend ostream& operator<<(ostream& outputStream, const CString& C1);

   CString& operator=(const CString& C1);

};

#endif
