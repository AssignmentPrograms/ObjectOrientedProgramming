/*
 * Name: Brandon Jones
 * WSU ID: N534H699
 * WSU Email ID: N534H699
 * */

#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
using namespace std;

class Matrix
{
   int numRows = 0; // number of rows
   int numCols = 0; // number of cols
   double** mArray = nullptr;
   
public:
   Matrix();
   
   // Copy Constructor
   Matrix(const Matrix& m2);
   
   // Deallocate space for a matrix
   const ~Matrix();
   
   // Adds two matrices together
   // TODO: RETURN BY REFERENCE TO ALLOW CASCADING?? nah, see req. sheet
   friend const Matrix operator+(const Matrix& m1, const Matrix& m2); //const;
   
   // Assignmnet Operator Overload
   const Matrix& operator=(const Matrix& m2);
   
   // Insertion Operator (ostream)
   friend ostream& operator<<(ostream& outputStream, const Matrix& m1);
   
   // Extraction Operator (istream)
   friend istream& operator>>(istream& inputStream, Matrix& m1);
   
   // Allocate space for a matrix
   const void allocateSpace();
   
   // Deallocate space for a matrix
   const void deallocateSpace();
};


#endif