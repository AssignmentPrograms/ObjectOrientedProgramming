/* Name: Brandon Jones
 * myWSU ID: N534H699
 * WSU Email: N534H699@wichita.edu
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;

void printNumbers(ifstream& inputFile, ofstream& outputFile);

int main()
{
   // The infileName is the holder for the input file name.
   // 12 characters plus space for the null character
   char infileName[13];
   
   // The outfileName is the holder for the output file name.
   // 12 characters plus space for the null character
   char outfileName[13];
   
   // Prompt user for the names of the respective files.
   cout << "Hello, please enter the name of the input file: ";
   cin >> infileName;
   cout << "Perfect, now please enter the name of the output file: ";
   cin >> outfileName;
   
   ifstream inputFile(infileName);
   /* Could also do:
    * ifstream inputFile;
    * inputFile.open(infileName);*/
   if(inputFile.fail()) { cerr << "Input File failed to open\n"; exit(1); }
   
   
   ofstream outputFile(outfileName);
   /* Could also do:
    * ofstream outputFile;
    * outputFile.open(outfileName);*/
   if(outputFile.fail()) { cerr << "Output File failed to open\n"; exit(1); }
   
   printNumbers(inputFile, outputFile);
   
   inputFile.close();
   outputFile.close();
   
   return 0;
}


void printNumbers(ifstream& inputFile, ofstream& outputFile)
{
   char format[3];
   //char width[3];
   int width;
   //char precision[3];
   int precision;
   
   double realNumber;
   
   //while(inputFile >> format)
   while(inputFile >> format)
   {
      // Read in the format from the file
      //inputFile >> format;
      
      // Read in the width
      inputFile >> width;
      
      // Read in the precision
      inputFile >> precision;
      
      // Read in the real number
      inputFile >> realNumber;
      
      // Determines if the format should be fixed or scientific
      if(format[0] == 'f'){
	 outputFile.unsetf(ios::scientific);
	 outputFile.setf(ios::fixed);
      }
      else{
	 outputFile.unsetf(ios::fixed);
	 outputFile.setf(ios::scientific);
      }
      // Detmines if we should show '+' or not
      if(format[1] == 'y')
	 outputFile.setf(ios::showpos);
      else
	 outputFile.unsetf(ios::showpos);
      // Determines if we should left/right align the output
      if(format[2] == 'l'){
	 outputFile.unsetf(ios::right);
	 outputFile.setf(ios::left);
      }
      else{
	 outputFile.unsetf(ios::left);
	 outputFile.setf(ios::right);
      }
      
      // Sets the width for the output line
      outputFile.width(width);
      // Sets the precision for the number
      outputFile.precision(precision);
      
      outputFile << realNumber;
      outputFile << '|';
      if( !inputFile.eof() )
	 outputFile << endl;
      //outputFile.unsetf(ios::right);
      //outputFile.unsetf(ios::scientific);
   }
}
