#include <iostream>
#include "DayOfYear.h"
#include <vector>
using namespace std;

// TODO: CORRECT THE OVERLOADS TO BE ABLE TO RETURN A REFERENCE VALUE APPROPRIATELY...

int main()
{
   // Declare 5 objects dy1--dy5 with any initial values;
   DayOfYear dy1(5, 6), dy2(3), dy3(7, 30), dy4(12, 5), dy5;
   
   // MUST use all 3 constructors
   // TODO: Eliminate the current default constructor, it may not be needed

   // Read in new values for all 5 objects using operator>>
   cout << "ENTER IN 5 DATES: " << endl << endl;
   cin >> dy1 >> dy2 >> dy3 >> dy4 >> dy5;

   // Output the values of all 5 objects using operator<<
   cout << endl << endl;
   cout << "dy1 = " << dy1;
   cout << "dy2 = " << dy2;
   cout << "dy3 = " << dy3;
   cout << "dy4 = " << dy4;
   cout << "dy5 = " << dy5 << endl;

   // Boolean variable used to compare dates
   string booleanOutcome;
   
   if( dy1 == dy2 ){ booleanOutcome = "true"; } else { booleanOutcome = "false"; }
   cout << "dy1 == dy2 is " << booleanOutcome << endl;
   
   if( dy1 < dy2 ){ booleanOutcome = "true"; } else { booleanOutcome = "false"; }
   cout << "dy1 < dy2 is " << booleanOutcome << endl;
   
   if( dy2 < dy1 ){ booleanOutcome = "true"; } else { booleanOutcome = "false"; }
   cout << "dy2 < dy1 is " << booleanOutcome << endl;
   cout << endl;
   
   cout << "dy1 + dy2 = " << dy1 + dy2;
   
    
   cout << "dy1 - dy2 = " << dy1 - dy2;
   cout << endl;
   
   cout << "- dy2 = " << -dy2 << endl;
   cout << endl;

   cout << "dy2[1] = " << dy2[1] << "  dy2[2] = " << dy2[2]
        << "  dy2[3] = " << dy2[3] << "  dy2[0] = " << dy2[0] << endl;
   cout << endl;

   cout << "dy1 pre-incremented thrice is " << ++(++(++dy1)) << endl;
   cout << "dy1 is " << dy1 << endl;
   cout << endl;

   cout << "dy3 post-incremented once is " << dy3++;
   cout << endl << "dy3 is " << dy3 << endl;
   cout << endl;

   // TODO: TRY THIS AND DELETE THE COMMENT
   // For your good: See what happens if you post-increment more than once

   cout << "dy2 pre-decremented thrice is " << --(--(--dy2)) << endl;
   cout << "dy2 is " << dy2 << endl;
   cout << endl;

   cout << "dy4 post-decremented once is " << dy4--;
   cout << endl << "dy4 is " << dy4 << endl;
   cout << endl;

   cout << "dy5 is " << dy5 << endl;
   cout << endl;

   // Declare a vector of 6 objects.
   vector<DayOfYear> dates(6);
   
   // Skip entry 0; set entries 1--5 to objects dy1--dy5.
   // Entry 0 is set to default value
   dates[1] = dy1;
   dates[2] = dy2;
   dates[3] = dy3;
   dates[4] = dy4;
   dates[5] = dy5;
   
   // Add an extra object (with value July 4) at the right end.
   dates.push_back( DayOfYear(7, 4) );
   
   // Output ALL the objects in the vector. Must use the size() function.
   cout << "DATES FROM VECTOR:" << endl << endl;
   
   for(int i = 0; i < dates.size(); i++)
   {
      cout << dates[i] << endl;
   }
   
}
