#ifndef DAYOFYEAR_H
#define DAYOFYEAR_H

#include <iostream>
using namespace std;

class DayOfYear
{
    int month = 1;
    int day = 1;
    void testDate() const;
    void readjust(int &monthValue, int &dayValue);

public:
    // Constructor that takes in a month and a day value
    DayOfYear(int monthValue, int dayValue);

    // Constructor that takes in a month value (initializes day to 1)
    DayOfYear(int monthValue);

    // Constructor that initalizes both month and day values to 1
    // TODO: Eliminate this constructor, not really needed;
    DayOfYear();

    // Allows for user to enter in a month and a day
    void input();

    // Print out the values of month and day in a paticular format
    //void output() const;

    // Return the object's month number
    int getMonthNumber() const;

    // Return the object's day number
    int getDay() const;
    
    int operator [](int index);

    DayOfYear operator +(const DayOfYear &date2) const;

    friend int operator -(DayOfYear &date);

    friend bool operator ==(DayOfYear &date1, DayOfYear &date2);

    friend bool operator <(DayOfYear &date1, DayOfYear &date2);
    
    // Prefix
    friend DayOfYear& operator ++(DayOfYear &date1);
    
    // Postfix
    const friend DayOfYear operator ++(DayOfYear &date1, int);

    // Prefix
    friend DayOfYear& operator --(DayOfYear &date1);
    
    // Postfix
    const friend DayOfYear operator --(DayOfYear &date1, int);

    friend ostream& operator <<(ostream& outputStream, DayOfYear date);
    
    friend istream& operator >>(istream& inputStream, DayOfYear& date);

};

int operator -(DayOfYear &date1, DayOfYear &date2);

#endif
