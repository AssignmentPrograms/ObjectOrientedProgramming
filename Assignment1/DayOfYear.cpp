#include <iostream>
#include <cstdlib>
#include "DayOfYear.h"
using namespace std;

void DayOfYear::readjust(int &monthValue, int &dayValue)
{
   if(dayValue < 1) { dayValue += 30; monthValue -= 1; }
   if(dayValue > 30) { dayValue -= 30; monthValue += 1; }
   if(monthValue < 1) { monthValue += 12; }
   if(monthValue > 12) { monthValue -= 12; }
}

// Constructor that takes in a month and a day value
DayOfYear::DayOfYear(int monthValue, int dayValue) : month(monthValue), day(dayValue)
{ testDate(); }

// Constructor that takes in a month value (initializes day to 1)
DayOfYear::DayOfYear(int monthValue) : DayOfYear(monthValue, 1)
{ testDate(); }

// Constructor that initalizes both month and day values to 1
DayOfYear::DayOfYear() : DayOfYear(1, 1)
{ testDate(); }

void DayOfYear::testDate() const
{
    if(month < 1 || month > 12)
    { cout << "Illegal Month Value\n"; exit(1); }
    if(day < 1 || day > 30)
    { cout << "Illegal Day Value\n"; exit(1); }
}

// Return the object's month number
int DayOfYear::getMonthNumber() const
{   return month; }


// Return the object's day number
int DayOfYear::getDay() const
{   return day; }

// Prefix
// GOOD
DayOfYear& operator ++(DayOfYear &date1)
{
   date1.month++;
   date1.day++;
   
   date1.readjust(date1.month, date1.day);
   
   return date1;
}

// Postfix
const DayOfYear operator ++(DayOfYear &date1, int)
{   
   int oldMonth = date1.month;
   int oldDay = date1.day;
   
   date1.month++;
   date1.day++;
   
   date1.readjust(date1.month, date1.day);
   
   return DayOfYear(oldMonth, oldDay);
}

// Prefix
DayOfYear& operator --(DayOfYear &date1)
{
   date1.month--;
   date1.day--;
   
   date1.readjust(date1.month, date1.day);
   
   return date1;
}

// Postfix
const DayOfYear operator --(DayOfYear &date1, int)
{
   DayOfYear old_DayOfYear = date1;
   
   date1.month--;
   date1.day--;
   
   date1.readjust(date1.month, date1.day);
   
   return old_DayOfYear;
}

DayOfYear DayOfYear::operator +(const DayOfYear &date2) const
{
    DayOfYear temp = date2;
    
    // Calculate the sum of the month values and the day values.
    int monthSum = month + date2.month;
    int daySum = day + date2.day;
    
    // Do any necessary readjustments
    temp.readjust(monthSum, daySum);
    
    //dateSum = DayOfYear(monthSum, daySum);
    
    return DayOfYear(monthSum, daySum);
}

int operator -(DayOfYear &date1, DayOfYear &date2)
{
    int monthDif = date1.getMonthNumber() - date2.getMonthNumber();
    
    int dayDif;
    int daysLeftInDate1;
    
    if( date1.getDay() > date2.getDay() )
    {
       dayDif = date1.getDay() - date2.getDay();
    }
    else
    {
       daysLeftInDate1 = 30 - date2.getDay();
       dayDif = date1.getDay() + daysLeftInDate1;
    }
    
    if(dayDif < 0) { dayDif *= -1; }
    int days_until_d1 = (monthDif * 30) + dayDif;

    if( date2 < date1 )
    {
       return days_until_d1;
    }
    else 
    {
        cout << "Error: Date 2 for this current year is not before Date 1\n";
        cout << "NOTE: It was said to assume that this would not happen, so I am returning -1 to satisify return condition\n";
	return -1;
    }
}

int operator -(DayOfYear &date)
{
    // Create a temporary DayOfYear object that contains month as 12 and day as 30
    DayOfYear d(12, 30);
    
    // Subtract the user specified date from the month=12 and day=30 object
    int dayDif = d.day - date.day;
    int monthDif = d.month - date.month;
    
    int day_diff = monthDif*30 + dayDif;
    //date = DayOfYear(monthDif, dayDif);

    return day_diff;
    //return date;
}

int DayOfYear::operator [](int index)
{
   if(index == 1) { return month; }
   if(index == 2) { return day; }
   if(index == 3) { return (month*30 + day); }
   return -1;
}

bool operator ==(DayOfYear &date1, DayOfYear &date2)
{
    return ( (date1.month == date2.month) && (date1.day == date2.day) );
}

bool operator <(DayOfYear &date1, DayOfYear &date2)
{
    return ( (date1.month < date2.month) || ( (date1.month == date2.month) && (date1.day < date2.day) ) );
}

// Print out the values of month and day in a paticular format
ostream& operator <<(ostream& outputStream, DayOfYear date)
{
   // Determine the current month in question
    switch(date.month)
    {
        case 1:
           outputStream << "(1 "; break; 
	   //outputStream << "January "; break;
        case 2:
	   outputStream << "(2 "; break; 
           //outputStream << "February "; break;
        case 3:
	   outputStream << "(3 "; break; 
           //outputStream << "March "; break;
        case 4:
	   outputStream << "(4 "; break; 
           //outputStream << "April "; break;
        case 5:
	   outputStream << "(5 "; break; 
           //outputStream << "May "; break;
        case 6:
	   outputStream << "(6 "; break; 
           //outputStream << "June "; break;
        case 7:
	   outputStream << "(7 "; break; 
           //outputStream << "July "; break;
        case 8:
	   outputStream << "(8 "; break; 
           //outputStream << "August "; break;
        case 9:
	   outputStream << "(9 "; break; 
           //outputStream << "September "; break;
        case 10:
	   outputStream << "(10 "; break; 
           //outputStream << "October "; break;
        case 11:
	   outputStream << "(11 "; break; 
           //outputStream << "November "; break;
        case 12:
	   outputStream << "(12 "; break; 
           //outputStream << "December "; break;
        default:
           outputStream << "Error in DayOfYear::output";
    }

    // Print out the day that goes with the selected month
    outputStream << date.day << ")" << endl;
    
    return outputStream;
}

// Allows for user to enter in a month and a day
istream& operator >>(istream& inputStream, DayOfYear& date)
{
    int month, day;
    cout << "Enter in the Month number: ";
    inputStream >> month;
    cout << "Enter in the Day number: ";
    inputStream >> day;
    
    date = DayOfYear(month, day);
    
    return inputStream;
}



